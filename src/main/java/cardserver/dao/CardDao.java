package cardserver.dao;

import cardserver.model.Card;

import java.util.List;

public interface CardDao {

    public Card getCardById(int id);

    public List<Card> listCards();
}
