package cardserver.dao;

import cardserver.model.Card;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CardDaoImpl implements CardDao {
    private static final Logger logger = LoggerFactory.getLogger(CardDaoImpl.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Card getCardById(int id) {
        Session session =this.sessionFactory.getCurrentSession();
        Card card = (Card) session.load(Card.class, new Integer(id));
        logger.info("Card successfully loaded. Card details: " + card);

        return card;
    }

    @Override
    @SuppressWarnings("unchecked")
   public List<Card> listCards() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Card> cardList = session.createQuery("from Card").list();

        for(Card card : cardList){
            logger.info("Card list: " + card);
        }

        return cardList;
    }
}
