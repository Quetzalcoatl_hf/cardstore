package cardserver.model;

import javax.persistence.*;

@Entity
@Table(name = "cards")
public class Card {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "CARD_TITLE")
    private String cardTitle;

    @Column(name = "CARD_AUTHOR")
    private String cardAuthor;

    @Column(name = "CARD_PRICE")
    private int price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCardTitle() {
        return cardTitle;
    }

    public void setCardTitle(String cardTitle) {
        this.cardTitle = cardTitle;
    }

    public String getCardAuthor() {
        return cardAuthor;
    }

    public void setCardAuthor(String cardAuthor) {
        this.cardAuthor = cardAuthor;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Card{" +
                "id=" + id +
                ", cardTitle='" + cardTitle + '\'' +
                ", cardAuthor='" + cardAuthor + '\'' +
                ", price=" + price +
                '}';
    }
}
