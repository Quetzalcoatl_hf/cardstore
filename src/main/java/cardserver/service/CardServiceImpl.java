package cardserver.service;

import cardserver.dao.CardDao;
import cardserver.model.Card;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CardServiceImpl implements CardService {
    private CardDao cardDao;

    public void setCardDao(CardDao cardDao) {
        this.cardDao = cardDao;
    }

    @Override
    @Transactional
    public Card getCardById(int id) {
        return this.cardDao.getCardById(id);
    }

    @Override
    @Transactional
    public List<Card> listCards() {
        return this.cardDao.listCards();
    }
}
