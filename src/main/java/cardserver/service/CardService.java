package cardserver.service;

import cardserver.model.Card;

import java.util.List;

public interface CardService {
    public Card getCardById(int id);

    public List<Card> listCards();
}
