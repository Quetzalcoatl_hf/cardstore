package cardserver.controller;

import cardserver.model.Card;
import cardserver.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CardController {
    private CardService cardService;

    @Autowired(required = true)
    @Qualifier(value = "CardService")
    public void setCardService(CardService cardService) {
        this.cardService = cardService;
    }

    @RequestMapping(value = "/cards", method = RequestMethod.GET)
    public String listCards(Model model){
        model.addAttribute("card", new Card());
        model.addAttribute("listCards", this.cardService.listCards());

        return "cards";
    }

    @RequestMapping("edit/{id}")
    public String editCard(@PathVariable("id") int id, Model model){
        model.addAttribute("card", this.cardService.getCardById(id));
        model.addAttribute("listCards", this.cardService.listCards());

        return "cards";
    }

    @RequestMapping("carddata/{id}")
    public String cardData(@PathVariable("id") int id, Model model){
        model.addAttribute("card", this.cardService.getCardById(id));

        return "carddata";
    }
}
