<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>
<html>
<head>
  <title>Cards|Card</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <!-- Bootstrap Core CSS -->
  <link href="../../css/bootstrap.css" rel='stylesheet' type='text/css' />
  <!-- Custom CSS -->
  <link href="../../css/style.css" rel='stylesheet' type='text/css' />
  <!-- font CSS -->
  <!-- font-awesome icons -->
  <link href="../../css/font-awesome.css" rel="stylesheet">
  <!-- //font-awesome icons -->
  <!-- js-->
  <script src="../../js/jquery-1.11.1.min.js"></script>
  <script src="../../js/modernizr.custom.js"></script>
  <!--webfonts-->
  <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
  <!--//webfonts-->
  <!--animate-->
  <link href="../../css/animate.css" rel="stylesheet" type="text/css" media="all">
  <script src="../../js/wow.min.js"></script>
  <script>
      new WOW().init();
  </script>
  <!--//end-animate-->
  <!-- chart -->
  <script src="../../js/Chart.js"></script>
  <!-- //chart -->
  <!--Calender-->
  <link rel="stylesheet" href="../css/clndr.css" type="text/css" />
  <script src="../../js/underscore-min.js" type="text/javascript"></script>
  <script src= "../../js/moment-2.2.1.js" type="text/javascript"></script>
  <script src="../../js/clndr.js" type="text/javascript"></script>
  <script src="../../js/site.js" type="text/javascript"></script>
  <!--End Calender-->
  <!-- Metis Menu -->
  <script src="../../js/metisMenu.min.js"></script>
  <script src="../../js/custom.js"></script>
  <link href="../../css/custom.css" rel="stylesheet">
  <!--//Metis Menu -->
</head>
<body class="">
<div class="main-content">

  <!-- header-starts -->
  <div class="sticky-header header-section ">
    <div class="header-left">

      <!--logo -->
      <div class="logo">
        <a href="../../index.jsp">
          <h1>CARD</h1>
          <span>server</span>
        </a>
      </div>
      <!--//logo-->
      <!--search-box-->
      <div class="search-box">
        <form class="input">
          <input class="sb-search-input input__field--madoka" placeholder="Search..." type="search" id="input-31" />
          <label class="input__label" for="input-31">
            <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
              <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
            </svg>
          </label>
        </form>
      </div><!--//end-search-box-->
      <div class="clearfix"> </div>
    </div>
    <div class="header-right">
      <div class="profile_details_left"><!--notifications of menu start -->
        <ul class="nofitications-dropdown">
          <li class="dropdown head-dpdn">
            <a href="../../index.jsp" class="dropdown-toggle" aria-expanded="false"><i class="fa fa-tasks"></i></a>
          </li>
          <li class="dropdown head-dpdn">
            <a href="../../index.jsp" class="dropdown-toggle" aria-expanded="false"><i class="fa fa-tasks"></i></a>
          </li>
          <li class="dropdown head-dpdn">
            <a href="../../index.jsp" class="dropdown-toggle" aria-expanded="false"><i class="fa fa-tasks"></i></a>
          </li>
        </ul>
        <div class="clearfix"> </div>
      </div>
      <!--notification menu end -->
      <div class="profile_details">
        <ul>
          <li class="dropdown profile_details_drop">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <div class="profile_img">
                <span class="prfil-img"><img src="images/a.png" alt=""> </span>
                <div class="user-name">
                  <p>UserName</p>
                  <span>UserRole</span>
                </div>
                <i class="fa fa-angle-down lnr"></i>
                <i class="fa fa-angle-up lnr"></i>
                <div class="clearfix"></div>
              </div>
            </a>
            <ul class="dropdown-menu drp-mnu">
              <li> <a href="../../index.jsp"><i class="fa fa-cog"></i> Settings</a> </li>
              <li> <a href="../../index.jsp"><i class="fa fa-user"></i> Profile</a> </li>
              <li> <a href="../../index.jsp"><i class="fa fa-sign-out"></i> Logout</a> </li>
            </ul>
          </li>
        </ul>
      </div>
      <div class="clearfix"> </div>
    </div>
    <div class="clearfix"> </div>
  </div>
  <!-- //header-ends -->

  <div id="page-wrapper">
    <div class="main-page general">
      <div class="tables">

        <div class="bs-example widget-shadow" data-example-id="bordered-table">

            <table class="table table-bordered">
              <tbody>

                <tr>
                  <td>${card.id}</td>
                  <td><img src="../../${card.cardTitle}.png">${card.cardTitle}</td>
                  <td>${card.cardAuthor}</td>
                  <td>${card.price/100}${card.price%100}</td>
                </tr>



              </tbody>
            </table>

        </div>

      </div>
    </div>
  </div>



</div>
<!-- Classie -->
<script src="../../js/classie.js"></script>
<script>
    var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
        showLeftPush = document.getElementById( 'showLeftPush' ),
        body = document.body;

    showLeftPush.onclick = function() {
        classie.toggle( this, 'active' );
        classie.toggle( body, 'cbp-spmenu-push-toright' );
        classie.toggle( menuLeft, 'cbp-spmenu-open' );
        disableOther( 'showLeftPush' );
    };


    function disableOther( button ) {
        if( button !== 'showLeftPush' ) {
            classie.toggle( showLeftPush, 'disabled' );
        }
    }
</script>
<!--scrolling js-->
<script src="../../js/jquery.nicescroll.js"></script>
<script src="../../js/scripts.js"></script>
<!--//scrolling js-->
<!-- Bootstrap Core JavaScript -->
<script src="../../js/bootstrap.js"> </script>
</body>
</html>


